import kotlinx.coroutines.cancelAndJoin
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main(args: Array<String>) = runBlocking<Unit> {
    val job = launch {
        try {
            repeat(1000) { i -> println("I'm sleeping $i ...")
                delay(200L)
            }
        } finally { println("I'm running finally") }
    }
    delay(1000L)
    println("main: I'm tired of watching!")
    job.cancelAndJoin()
    println("main: Now I can quit")
}
