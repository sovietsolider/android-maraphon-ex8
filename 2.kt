import kotlinx.coroutines.*
import kotlin.system.*


suspend fun job1(): Int {
    delay(500L)
    return 1
}

suspend fun job2(): Int {
    delay(500L)
    return 2
}

fun main() = runBlocking<Unit> {
    var elapsedTime = measureTimeMillis {
        val first = job1()
        val second = job2()
        println(first + second)
    }
    println("Elapsed timed, calling in turn : $elapsedTime")
    elapsedTime = measureTimeMillis {
        val first_2 = async { job1() }
        val second_2 = async { job2() }
        println(first_2.await()+second_2.await())
    }
    println("Elapsed timed, async call : $elapsedTime")
}

/*
    Result: Elapsed timed, calling in turn : 1011
            Elapsed timed, async call : 521

    Второй код выполняется примерно в два раза быстрее, т.к корутины вызываются одновременно,
    а не последовательно.
*/


